﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Services;

namespace OnlineResumePortal.Controllers
{
    [CustomAuthorize]
    public class HomeController : Controller
    {
        ServicesBl serviceBl = new ServicesBl();
        // GET: Home
        public ActionResult Index()
        {
            var loggedUser = (Services.Model.ResumeModel)HttpContext.Session?["LoggedUser"];
            if (string.IsNullOrEmpty(loggedUser.ProfileImageName) || string.IsNullOrEmpty(loggedUser.PublicName) || string.IsNullOrEmpty(loggedUser.FullName))
            {
                var getInfo = serviceBl.GetSavedDetails(loggedUser.Username);
                Session["LoggedUser"] = getInfo;
            }
            return View();
        }
    }
}