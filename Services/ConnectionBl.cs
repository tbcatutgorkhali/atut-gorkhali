using MongoDB.Driver;
using Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public static class ConnectionBl
    {
        public static IMongoCollection<ResumeModel> MongoDbConnection()
        {
            MongoClient client = new MongoClient();
            var db = client.GetDatabase("ResumePORTAL");
            var collection = db.GetCollection<ResumeModel>("ResumeAccounts");
            return collection;
        }

    }
}
                                  